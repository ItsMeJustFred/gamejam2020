﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterControl : MonoBehaviour
{
    private bool grounded=false;
    public Vector2 groundCheckSize;
    public Vector3 groundCheckOffset;

    private float horMove;
    private float horInput;
    private float prevDir;
    public float groundRunSpeed;
    public float groundAcc;
    public float groundDec;

    private float verMove;
    private bool tryingToJump = false;
    public float gravity;
    public float jumpPower;
    public float jumpBufferTime;

    public bool caught = false;
    public int amountOfSmashes;
    public float longestDelayBetweenSmashes;
    private int currentSmashes;
    private float currentTimeSinceSmash;
    private GameObject gameCursor; 

    public float deadZone;

    public bool doubleJumped = false;

    private Rigidbody2D rb;

    
                               ////////////// GROUND CHECK /////////////////


    private bool groundCheck()
    {
        Collider2D[] cols = Physics2D.OverlapBoxAll(transform.position + groundCheckOffset, groundCheckSize,0f);

        foreach(Collider2D col in cols)
        {
            if(col.tag == "CanStand")
            {
                return (true);
            }
        }

        return (false);
    }

                                 ////////////// ACCELERATE /////////////////
   
    private float accelerate(float dir, float accAmount)
    {

        if (horInput < 0)
        {
            prevDir = -1f;
        }
        else
        {
            prevDir = 1f;
        }

        float speed = rb.velocity.x;

        if(Mathf.Abs(speed) >= Mathf.Abs(groundRunSpeed*dir))
        {
            speed = groundRunSpeed * dir;
        }
        else
        {
            speed = Mathf.Clamp(speed + (accAmount * dir * Time.deltaTime), -groundRunSpeed, groundRunSpeed);
        }

        return (speed);
    }

                                      ////////////// DECELERATE /////////////////

    private float decelerate(float dir, float decAmount)
    {
        float speed = rb.velocity.x;

        if(speed * dir <= 0) //decelerated too much
        {
            speed = 0;
        }
        else
        {
            speed = Mathf.Clamp(speed - (decAmount * dir * Time.deltaTime), -groundRunSpeed, groundRunSpeed);
        }

        return (speed);
    }


                                      ////////////// JUMP BUFFER /////////////////

    private IEnumerator jumpBuffer()
    {
        yield return new WaitForSeconds(jumpBufferTime);
        tryingToJump = false;
    }



                                       ////////////// START /////////////////


    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        gameCursor = GameObject.Find("gameCursor");
    }


                                     ////////////// UPDATE /////////////////

    private void Update()
    {
        if (!caught)
        {
            currentSmashes = 0;
            currentTimeSinceSmash = 0;
            horInput = Input.GetAxis("Horizontal");

            if (Input.GetButtonDown("Jump"))
            {
                StopCoroutine(jumpBuffer());
                tryingToJump = true;
                StartCoroutine(jumpBuffer());
            }

            if (groundCheck())
            {
                if (Mathf.Abs(horInput) >= deadZone)
                {
                    //move or accelerate
                    //put the quick change in here
                    horMove = accelerate(horInput, groundAcc);
                }
                else
                {
                    horMove = decelerate(prevDir, groundDec);
                }
            }
            else
            {
                if (Mathf.Abs(horInput) >= deadZone)
                {
                    horMove = accelerate(horInput, groundAcc * 3);
                }
                else
                {
                    horMove = decelerate(prevDir, 0f);
                }
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
            transform.position = gameCursor.transform.position;
            if (Input.GetButtonDown("Jump"))
            {
                currentSmashes++;
            }
            else
            {
                currentTimeSinceSmash += Time.deltaTime;
            }


            if (currentSmashes >= amountOfSmashes)
            {
                //break free
                Debug.Log("freeee");
                caught = false;
            }
            else if(currentTimeSinceSmash>= longestDelayBetweenSmashes)
            {
                currentSmashes/=2;
            }
        }
    }

    public void gotCaught()
    {
        caught = true;
        Debug.Log("Caught");
    }

                                    ////////////// FIXED UPDATE /////////////////

    void FixedUpdate()
    {
        if (!caught)
        {
            if (groundCheck())
            {
                doubleJumped = false;
                verMove = 0f;
                if (tryingToJump)
                {
                    verMove = jumpPower;
                    tryingToJump = false;
                }
            }
            else
            {
                if (tryingToJump && !doubleJumped)
                {
                    verMove = jumpPower;
                    tryingToJump = false;
                    doubleJumped = true;
                }
                verMove -= gravity;
            }



            rb.velocity = new Vector2(horMove, verMove);
        }
    }


    ////////////// GIZMOS /////////////////

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position+groundCheckOffset, groundCheckSize);
    }
}
