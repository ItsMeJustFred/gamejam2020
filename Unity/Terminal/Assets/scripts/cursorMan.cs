﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cursorMan : MonoBehaviour
{
    public float cursorSpeed;
    public float attackSpeed;
    public float timeBetweenAttacks;
    public float playerInfluence;

    public float randomAmount;

    public float minTimeBetweenAttacks;
    public float maxTimeBetweenAttacks;

    public float targetRadius;
    public float changeAngle;
    private GameObject player;
    private bool pickedPoint = false;
    private float r;
    private int direction=1;

    private bool waitingToAttack = true;
    private bool attacking = false;

    public float mouseDeadZone = 0.1f;
    private Vector2 prevMousePos;

    private Rigidbody2D rb;
    private SpriteRenderer sr;

    private float screenAffect;
    private bool coroutineStarted=false;
    private Vector2 mouseVelocity;
    void Start()
    {
        player = GameObject.Find("player");
          float r = Random.Range(minTimeBetweenAttacks, maxTimeBetweenAttacks);
          StartCoroutine(attackCounter(r));

        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        screenAffect = Screen.width / 840f;
    }


    void FixedUpdate()
    {
       Cursor.visible = false;
       mouseVelocity = Vector2.zero;
       
                    if (Input.mousePosition.x > prevMousePos.x + mouseDeadZone)
                    {
                        // mouse moving right
                        mouseVelocity += new Vector2(1f, 0f);
                    }
                    else if (Input.mousePosition.x < prevMousePos.x - mouseDeadZone)
                    {
                        // mouse moving left
                        mouseVelocity += new Vector2(-1f, 0f);

                    }

                    if (Input.mousePosition.y > prevMousePos.y + mouseDeadZone)
                    {
                        // mouse moving up
                        mouseVelocity += new Vector2(0f, 1f);


                    }
                    else if (Input.mousePosition.y < prevMousePos.y - mouseDeadZone)
                    {
                        // mouse moving down
                        mouseVelocity += new Vector2(0f, -1f);

                    }


                    Vector2 move = Vector3.Normalize(player.transform.position - this.transform.position) * cursorSpeed;

                    if (mouseVelocity != Vector2.zero)
                    {
                        Cursor.lockState = CursorLockMode.None;
                        rb.velocity = Vector2.Lerp(rb.velocity, mouseVelocity * playerInfluence * screenAffect, 0.1f);
                    }
                    else
                    {
                        Debug.Log("funky");
                        Cursor.lockState = CursorLockMode.Locked;
                        rb.velocity = move;
                    }
        prevMousePos = Input.mousePosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == player)
        {
            player.SendMessage("gotCaught");
            Camera.main.SendMessage("gotCaught");
            player.transform.parent = this.transform;

        }
    }


    private IEnumerator attackCounter(float time)
    {
        yield return new WaitForSeconds(time);
        attacking = true;
    }
}
