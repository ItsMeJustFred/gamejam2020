﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    private Transform player;

    public Vector3 offset;

    public float lerpAmount;
    private bool caught = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!caught)
        {
            transform.position = Vector3.Lerp(this.transform.position, player.position + offset, lerpAmount);
        }
    }

    public void gotCaught()
    {
        caught = true;
    }

}
